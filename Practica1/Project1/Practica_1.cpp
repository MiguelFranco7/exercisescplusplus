#include <cstdio>
#include "Practica_1.h"

/**
	�	Los 8 bits de mayor peso contienen la vida del personaje
	�	Los siguientes 8 bits el n�mero de balas.
	�	Los siguientes 4 bits el n�mero de compa�eros.
	�	El bit 0 el indicador de modo invulnerable.
	�	El bit 1 el indicador de balas infinitas
	�	El bit 2 el indicador de escudo presente.
	�	El bit 3 el indicador de modo �berseker�.
	    vvvv vvvv bbbb bbbb cccc xxxx xxxx rein
**/

int main(int argc, char * argv[]) {
	int infoPersonaje = 0x00590000;

	printf("Numero de balas: %d \n", getNumBalas(infoPersonaje));
	addBalas(infoPersonaje, 24);
	printf("Numero de balas tras a�adir: %d \n", getNumBalas(infoPersonaje));

	printf("Modo de balas infinitas: %d \n", getModoBalasInfinitas(infoPersonaje));
	activarBalasInfinitas(infoPersonaje);
	printf("Modo de balas infinitas tras ser activado: %d \n", getModoBalasInfinitas(infoPersonaje));

	getchar();

	return 0;
}

int getNumBalas(int info) {
	int numBalas = info & BITNUMBALAS;

	numBalas >>= 16;

	return numBalas;
}

void addBalas(int &info, int numBalas) {
	int totalBalas = getNumBalas(info) + numBalas;

	totalBalas <<= 16;

	info &= ~BITNUMBALAS;

	info |= totalBalas;
}

bool getModoBalasInfinitas(int info) {
	int balas = info & BIT1;

	balas >>= 1;

	return balas;
}

void activarBalasInfinitas(int &info) {
	info |= BIT1;
}