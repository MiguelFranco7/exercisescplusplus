#define BIT1 0x00000002		   // Balas infinitas.
#define BITNUMBALAS 0x00FF0000 // Balas.

int  getNumBalas		   (int info);
void addBalas			   (int &info, int numBalas);
bool getModoBalasInfinitas (int info);
void activarBalasInfinitas (int &info);
