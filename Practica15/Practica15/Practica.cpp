#include "stdafx.h"

class CBase {
public:
	CBase() {
		printf("CBase()\n");
		MetodoV1();
	}

	virtual ~CBase() {
		printf("~CBase()\n");
	}

	virtual void MetodoV1() {
		printf("CBase::MetodoV1()\n");
	}

	virtual void MetodoV2() {
		printf("CBase::MetodoV2()\n");
	}

	void Metodo() {
		printf("CBase::Metodo()\n");
	}
};

class CDerived : public CBase {
	public:
	CDerived() {
		printf("CDerived()\n");
		MetodoV1();
	}

	~CDerived() {
		printf("~CDerived()\n");
	}

	virtual void MetodoV1() {
		printf("CDerived::MetodoV1()\n");
	}

	virtual void MetodoV2() {
		printf("CDerived::MetodoV2()\n");
	}

	void Metodo() {
		printf("CDerived::Metodo()\n");
	}
};

class CDerived2 : public CBase {
	public:
	CDerived2() {
		printf("CDerived2()\n");
		MetodoV1();
	}

	~CDerived2() {
		printf("~CDerived2()\n");
	}

	virtual void MetodoV2() {
		printf("CDerived::MetodoV2_2()\n");
	}
};

int main() {
	CBase	 *pBase	    = new CBase();
	CBase	 *pBaseD1	= new CDerived();
	CBase	 *pBaseD2   = new CDerived2();

	// Llamada a m�todos.
	pBaseD1->MetodoV1();
	pBaseD1->Metodo();

	// *** RESPUESTAS: ***

	/*  1 - Por cada funci�n virtual ocupa 4 bytes en plataformas de 32 bits y 8 bytes en plataformas de 64 bits ya que es lo que ocupa una direcci�n, por lo que la tabla ocupar� seg�n el n�mero de funciones virtuales que tenga(ejemplo: si hay 2 funciones virtuales, la tabla ocupar� 4bytes * 2funciones virtuales).
		2 - La tabla est� situada en memoria, que es creada en tiempo de compilaci�n.Esta tabla s�lo tiene acceso de lectura ya que no va a ser sobrescrita.
		3 - Adicionalmente ocupa lo que ocupa un puntero(4 u 8 bytes seg�n la plataforma).Ya que s�lo se guarda el puntero que apunta a la direcci�n de la tabla en memoria.
		4 - Al llamar a un m�todo virtual desde el constructor se llama al m�todo virtual de la clase del constructor(o del padre si no esta implementado siendo una derivada).Si llamas al constructor de una clase tiene sentido llamar a los m�todos virtuales de dicha clase ya que el hijo puede depender de esas acciones del padre como por ejemplo inicializaciones.
		5 - La funci�n virtual a la que se llama es a la de la derivada, en cambio la no virtual se llama al del padre(el tipo de objeto que es).No hay pasos adicionales, ya que tiene la direcci�n de memoria del m�todo a llamar.*/

	getchar();

	// Borrado desde la clase base. Destructores virtuales
	delete pBase;
	delete pBaseD1;
	delete pBaseD2;

	return 0;
}

