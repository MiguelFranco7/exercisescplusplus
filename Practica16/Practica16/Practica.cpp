// Practica16.cpp

#include "stdafx.h"

class CStream {
public:
	CStream() {
		printf("CStream()\n");
		m_iValueBase = 0;
	}

	virtual ~CStream() {
		printf("~CStream()\n");
	}

	virtual void StreamOpen() = 0;
	virtual void StreamClose() = 0;
	virtual void StreamRead() = 0;
	virtual void StreamWrite() = 0;

private:
	int m_iValueBase;
};

class CFile : public CStream {
public:
	CFile() {
		printf("CFile()\n");
		m_iValueDerived = 0;
	}

	~CFile() {
		printf("~CFile()\n");
	}

	virtual void StreamOpen() {
		printf("CFile open.\n");
	}

	virtual void StreamClose() {
		printf("CFile close.\n");
	}

	virtual void StreamRead() {
		printf("CFile read.\n");
	}

	virtual void StreamWrite() {
		printf("CFile write.\n");
	}

private:
	int m_iValueDerived;
};

class CCom : public CStream {
public:
	CCom() {
		printf("CCom()\n");
		m_iValueDerived = 0;
	}

	~CCom() {
		printf("~CCom()\n");
	}

	virtual void StreamOpen() {
		printf("CCom open.\n");
	}

	virtual void StreamClose() {
		printf("CCom close.\n");
	}

	virtual void StreamRead() {
		printf("CCom read.\n");
	}

	virtual void StreamWrite() {
		printf("CCom write.\n");
	}

private:
	int m_iValueDerived;
};

class CTcp : public CStream {
public:
	CTcp() {
		printf("CTcp()\n");
		m_iValueDerived = 0;
	}

	~CTcp() {
		printf("~CTcp()\n");
	}

	virtual void StreamOpen() {
		printf("CTcp open.\n");
	}

	virtual void StreamClose() {
		printf("CTcp close.\n");
	}

	virtual void StreamRead() {
		printf("CTcp read.\n");
	}

	virtual void StreamWrite() {
		printf("CTcp write.\n");
	}

private:
	int m_iValueDerived;
};

int _tmain(int argc, _TCHAR* argv[]) {
	//CStream *pBase = new CStream();
	CStream *pStreamF = new CFile();
	CStream *pStreamC = new CCom();
	CStream *pStreamT = new CTcp();
	CFile *pFile = new CFile();
	CCom *pCom = new CCom();
	CTcp *pTcp = new CTcp();

	// Llamada a m�todos virtuales
	pStreamF->StreamOpen();
	pStreamC->StreamOpen();
	pStreamT->StreamOpen();
	pFile->StreamRead();
	pCom->StreamRead();
	pTcp->StreamRead();

	// Borrado desde la clase base. Destructores virtuales
	delete pStreamF;
	delete pStreamC;
	delete pStreamT;
	delete pFile;
	delete pCom;
	delete pTcp;

	getchar();

	return 0;
}

