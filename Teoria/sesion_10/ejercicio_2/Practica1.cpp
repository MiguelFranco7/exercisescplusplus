// Practica23.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "TLista.h"

template<class T>void PrintElements(T& itIni, const T& itEnd);

void __cdecl f();
void __stdcall f2();

int _tmain(int argc, _TCHAR* argv[])
{
	TList<char *> Lista;
	Lista.AddLast("Hola ");
	Lista.AddLast("Hola ");
	Lista.AddLast("Caracola");

	/*
	TList<char *>::TIterator it = Lista.begin();
	while (!(it == Lista.end()))
	{
		printf("%s", *it);
		++it;
	}
	*/

	PrintElements(Lista.begin(), Lista.end());

	getchar();

	return 0;
}

template<class T>void PrintElements(T& itIni, const T& itEnd)
{
	while (!(itIni == itEnd))
	{
		printf("%s", *itIni);
		++itIni;
	}
}