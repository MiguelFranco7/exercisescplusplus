// Practica23_2.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <list>
#include <map>
#include <string>

void func(int i)
{
	printf("%d", i);
}

int _tmain(int argc, _TCHAR* argv[])
{
	std::list<const char *> lista;

	lista.push_back("Hola");
	lista.push_back("aa");
	lista.push_back("Caracola");
	lista.push_back("prueba aa");
	lista.push_back("Tomas");
	lista.push_back("aa prueba");
	
	int i = 0;
	func(i++);

	std::list<const char *>::iterator it = lista.begin();
	while (it != lista.end())
	{
		if (strstr(*it, "aa"))
		{
			/*
			std::list<const char *>::iterator itAux = it;
			it++;
			lista.erase(itAux);
			*/
			lista.erase(it++);
		}else
			it++;
	}
	
	std::map<int, const char *> mapa;
	
	mapa.insert(std::pair<int, const char *>(0, "Cadena0"));
	mapa.insert(std::pair<int, const char *>(1, "Cadena1"));
	mapa.insert(std::pair<int, const char *>(2, "Cadena2"));

	std::map<int, const char *>::iterator itmap = mapa.find(1);
	printf("Result: %d-%s", (*itmap).first, (*itmap).second);
	mapa.erase(itmap);
	return 0;
}

