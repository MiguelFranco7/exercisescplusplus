#include "stdafx.h"

// ***********************************************************************
// Clase creation
// ***********************************************************************
template <class T> class TList
{
public:
	TList();
	~TList();

	void AddLast		(const T& Item);
	T* GetFirstItem	();
	T* GetNextItem	();
	void DeleteFisrt();
	unsigned int GetSize() const;

private:
	struct TElement 
	{
		TElement(const T& Item)
		{
			m_Item					= Item;
			m_pNextElement	= NULL;
		}
		T					m_Item;
		TElement *m_pNextElement;
	};

	TElement *m_pFisrtElement;
	TElement *m_pLastElement;

	TElement *m_pCurrent;

	unsigned int m_iSize;
};


// ***********************************************************************
// Constructor
// ***********************************************************************
template<class T> TList<T>::TList()
{
	m_pLastElement = m_pFisrtElement = m_pCurrent = NULL;
	m_iSize = 0;
}

// ***********************************************************************
// Destructor
// ***********************************************************************
template<class T> TList<T>::~TList()
{
	while (m_pFisrtElement)
	{
		TElement *pNextElement = m_pFisrtElement->m_pNextElement;
		delete m_pFisrtElement;
		m_pFisrtElement = pNextElement;
	}
	m_pFisrtElement = m_pLastElement = NULL;
}

// ***********************************************************************
// AddLast
// ***********************************************************************
template<class T> void TList<T>::AddLast(const T &Item)
{
	if (!m_pLastElement)
		m_pFisrtElement = m_pLastElement = new TElement(Item);
	else
	{
		m_pLastElement->m_pNextElement = new TElement(Item);
		m_pLastElement = m_pLastElement->m_pNextElement;
	}
	m_iSize++;
}

// ***********************************************************************
// Get items
// ***********************************************************************
template<class T> T* TList<T>::GetFirstItem()
{
	T* ret = NULL;
	m_pCurrent = m_pFisrtElement;
	if (m_pCurrent)
	{
		ret = &m_pCurrent->m_Item;
		m_pCurrent = m_pCurrent->m_pNextElement;
	}
	return ret;
}

template<class T> T* TList<T>::GetNextItem()
{
	T* ret = NULL;
	if (m_pCurrent)
	{
		ret = &m_pCurrent->m_Item;
		m_pCurrent = m_pCurrent->m_pNextElement;
	}
	return ret;
}

template<class T>	void TList<T>::DeleteFisrt()
{
	if (m_pFisrtElement)
	{
		if (m_pCurrent == m_pFisrtElement)
			m_pCurrent = m_pFisrtElement->m_pNextElement;

		if (m_pLastElement == m_pFisrtElement)
			m_pLastElement = NULL;

		TElement *pAux = m_pFisrtElement;
		m_pFisrtElement = m_pFisrtElement->m_pNextElement;
		delete pAux;
		m_iSize--;
	}
}

template<class T>	unsigned int TList<T>::GetSize() const
{
	return m_iSize;
}


// ***********************************************************************
// Funci�n que retorna un array con los elementos de la lista
// ***********************************************************************
template<class T> T* GetArray(TList<T> &lstSrc)
{
	unsigned int uSize	= lstSrc.GetSize();
  T *pArray = new T[uSize];
	T*pItem = lstSrc.GetFirstItem();
	int i = 0;
	while(pItem)
	{
		pArray[i++] = *pItem;
		pItem = lstSrc.GetNextItem();
	}
	return pArray;
}