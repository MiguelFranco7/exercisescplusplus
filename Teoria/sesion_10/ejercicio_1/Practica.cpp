// Practica20.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "TLista.h"

// *********************************************************************
class CObserver;
template <class T> class CEventSource
{
public:
	
	CEventSource(T pListener)
	{
		m_pListener = pListener;
	}

	void Run()
	{
		m_pListener->ManageEvent();
	}
private:
	T m_pListener;
};

// *********************************************************************
class CObserver
{
public:
	CObserver()
	{
		m_pEventSource = NULL;
	}

	void Init()
	{
		m_pEventSource = new CEventSource<CObserver *>(this);
		m_pEventSource->Run();
	}

	void ManageEvent()
	{
		printf("CListener::ManageEvent\n");
	}
private:
	CEventSource<CObserver *> *m_pEventSource;
};


// *********************************************************************
//
// *********************************************************************
int _tmain(int argc, _TCHAR* argv[])
{
	// *****************************************************
	// Pr�ctica de lista.
	// *****************************************************

	// Caracteres
	TList<char> ListaChar;
	ListaChar.AddLast('H');
	ListaChar.AddLast('o');
	ListaChar.AddLast('l');
	ListaChar.AddLast('a');

	char *pChar = ListaChar.GetFirstItem();
	while (pChar)
	{
		printf("%c", *pChar);
		pChar = ListaChar.GetNextItem();
	}
	printf("\n");

	// Cadenas
	TList<char *> ListaStrings;
	ListaStrings.AddLast("Hola ");
	ListaStrings.AddLast("Hola ");
	ListaStrings.AddLast("Caracola");

	char **pStr = ListaStrings.GetFirstItem();
	while (pStr)
	{
		printf("%s", *pStr);
		pStr = ListaStrings.GetNextItem();
	}
	printf("\n");

	// Lista de listas. OJO compila pero nu funciona
	TList<TList<char *>> ListaListaStrings;
	ListaListaStrings.AddLast(ListaStrings);

	void(TList<TList<char *>>::*f1)(const TList<char *>&) = &TList<TList<char *>>::AddLast;
	void(TList<char *>::*f2)(char * const&) = &TList<char *>::AddLast;
	void(TList<char>::*f3)(const char &) = &TList<char>::AddLast;

	// Funci�n template
	char *pCharArray = GetArray(ListaChar);
	//char *pCharArray = GetArray<char>(ListaChar);

	int iLast = ListaChar.GetSize() - 1;
	for (int i = iLast; i >= 0; i--)
		printf("%c", pCharArray[i]);

	printf("\n");

	// Patr�n del observador con templates
	CObserver oObjerver;
	oObjerver.Init();

	getchar();

	return 0;
}

