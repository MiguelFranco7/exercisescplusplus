// Practica.cpp : Defines the entry point for the console application.
//
#include <stdio.h>

int main(int argc, char* argv[])
{
	int Var;
	int Var2;
	int &RefVar = Var;
	int *pVar		= &Var;
	char TablaM[2][3][4][5];
	char TablaM2[4][5];

	// Funcionamiento de las referencias y puneteros
	Var			= 1;
	RefVar	= 2;
	*pVar		= 3;

	// Tablas punteros y referencias:
	int iCount = 0;
	for (int i = 0; i < 2; i++)
		for (int j = 0; j < 3; j++)
			for (int k = 0; k < 4; k++)
				for (int l = 0; l < 5; l++)
					TablaM[i][j][k][l] = iCount++;

	iCount = 0;
	for (int i = 0; i< 4; i++)
		for (int j = 0; j < 5; j++)
			TablaM2[i][j] = iCount++;

	char cVar1			= TablaM2[2][3];
	char *pTablaM2	= &TablaM2[0][0];
	char cVar2			= *(pTablaM2 + (2 * 5) + 3);

	// Punteros a cadenas constantes
	const char *pConts = "kkk";
//	char * pstr		= pConts;
//	pstr[0]= '\0';
  char * pstr = "ADIOS"; // Por compativilidad admite una asignaci�n con puntero no const
//	pstr[0]= '\0'; // Error de ejecuci�n, acceso a una memoria de s�lo lectura

  int i = 3;
	int const *puntero1 = &i;
	const int *puntero2 = &i;
	int *const puntero3 = &i;
//	*puntero1 = 2;
//	*puntero2 = 2;
	*puntero3 = 2;
//	*puntero3 = puntero1;

	char	Tabla[256] = "HOLA";
	char *pPointer = Tabla;

	Tabla[0]    = '\0';
	*Tabla      = '\0';
	*pPointer   = '\0';
	pPointer[0] = '\0';

	// Funcionamiento de los casting
	int   iVal  = 0xFFFFFFFF;
	float fVal	= 3.1416;
	char *pVal  = nullptr;
	char  cChar = 0xFF;
	int * pInt	= &iVal;
	void *pVoid = nullptr;
  unsigned u;
  float    f;

	pInt = static_cast<int *>   (pVoid);
  u    = static_cast<unsigned>(iVal);
  f    = static_cast<float>   (u);

	pVal = reinterpret_cast<char *>(pInt);
	//pVal = static_cast<char *>(pInt);

	iVal = static_cast<int>(fVal);
	//iVal = reinterpret_cast<int>(fVal);

	pVal  = reinterpret_cast<char *>(iVal);
	iVal  = reinterpret_cast<int>   (pVal);
	cChar = reinterpret_cast<char > (pVal);
	pVal  = reinterpret_cast<char *>(cChar);

	printf("%d-%d\n", sizeof(iVal), sizeof(fVal));

  int i2 = 3;
  int &iRef = i2;
  iRef = i;

  i2   = 1;
  iRef = 1;

	getchar();

	// PR�CTICAS.
	// 1.- Dado un entero obtener por separado cada uno de sus bytes usando operaciones de punteros. 
	// 2.- Dada la tabla de enteros int tabla[] = {1, 3, 2, 5, 3, 0xFFFFFFFF, 2}
	//		 Realizar c�digo que obtenga el entero mayor usando aritm�tica de punteros.
	// 3.- Dada la tabla de enteros char tabla[] = {'a', 'z', 'f', 'g', '2'}
	//		 Realizar c�digo que obtenga el caracter mayor usando aritm�tica de punteros.
	// 4.- Igual que el 2 pero que obtenga la posici�n del byte mayor.
	// 5.- Generar c�digo que dada una cadena le de la vuelta: ejmp "hola" -> "aloh"


	return 0;
}

