// Practica14.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"



#include "Jpg.h"
#include "Png.h"

void PrintImageInfo(const CImagen &oImagen);

int _tmain(int argc, _TCHAR* argv[])
{
	// Pr�ctica Imagen
	CPng oPng;
	CJpg oJpg;

	oPng.SetPngFile("Fichero.png");
	oJpg.SetJpgFile("Fichero.jpg");

	PrintImageInfo(oPng);
	PrintImageInfo(oJpg);

	getchar();

	return 0;
}

// ***************************************************
//
// ***************************************************
void PrintImageInfo(const CImagen &oImagen)
{
	printf("NumPixes: %d \nSize: %d\n", oImagen.GetNumPixels(), oImagen.GetUncompressedSize());
}