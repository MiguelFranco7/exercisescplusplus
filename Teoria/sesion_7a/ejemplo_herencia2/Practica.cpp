// Practica15.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"

class CBase
{
public:

	CBase()
	{
		m_iBase = 0x1;
		printf("CBase()\n");
	}

	~CBase()
	{
		printf("~CBase()\n");
	}

	void MetodoBase()
	{
		printf("MetodoBase()\n");
	}

int m_iBase;
};

class CDerivada : public CBase
{
public:
	CDerivada()
	{
		m_iDerivada = 0xFFFFFFFF;
		printf("CDerivada()\n");
	}

	~CDerivada()
	{
		printf("~CDerivada()\n");
	}
								 
	void MetodoDerivado()
	{
		printf("MetodoDerivado()\n");
	}

	int m_iDerivada;
};

class CDerivada1_2 : public CDerivada
{
public:
	CDerivada1_2()
	{
		printf("CDerivada1_2()\n");
	}

	~CDerivada1_2()
	{
		printf("~CDerivada1_2()\n");
	}
};

class CDerivada2 : public CBase
{
public:
	CDerivada2()
	{
		printf("CDerivada2()\n");
	}

	~CDerivada2()
	{
		printf("~CDerivada2()\n");
	}
};

int _tmain(int argc, _TCHAR* argv[])
{
	{ // Orden de ejecuci�n de constructores y destructores.
		CDerivada1_2 oObj;
	}

	// Asignaci�n entre punteros y variables de la base la derivada
	CDerivada oDerivada;
	CBase			oBase;
	CDerivada *pDerivada	= &oDerivada;
	CBase			*pBase			= NULL;

	pBase			= pDerivada;
	// pDerivada = pBase;			// La derivada es del tipo base, pero el tipo base no es del tipo derivado

	// Sabemos que el puntero de la base es de tipo derivada luego es segura la conversi�n
	// IMPORTANTE notar que despu�s de la asignacion vuelven a ser accesibles los miembros de la clase derivada.
	pDerivada = static_cast<CDerivada *>(pBase);  // Mejor utilizar static_cast por que los tipos son compatibles
	//pDerivada = reinterpret_cast<CDerivada *>(pBase); 
	pDerivada->MetodoDerivado();

	oBase			= oDerivada;	// El compilador crea un operador por defecto para realizar la asiganci�n de la parte com�n
	// oDerivada = oBase;		// ERROR no hay operador (tampoco por defecto) para realizara la asignaci�n

	// �Y si forzamos la conversi�n de un puentro a una clase base que no es de la derivada a la derivada?
	pDerivada = reinterpret_cast<CDerivada *>(&oBase); 
	pDerivada->MetodoDerivado();
	printf("pDerivada->m_iDerivada: %d\n", pDerivada->m_iDerivada);

	return 0;
}

