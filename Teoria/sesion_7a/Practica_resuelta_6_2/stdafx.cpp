// stdafx.cpp : source file that includes just the standard includes
// Practica12.pch will be the pre-compiled header
// stdafx.obj will contain the pre-compiled type information

#include "stdafx.h"
#include <stdlib.h>
// TODO: reference any additional headers you need in STDAFX.H
// and not in this file
void* operator new (unsigned int uSize)
{
	return malloc(uSize);
}

void operator delete (void* ptr)
{
	free(ptr);
}