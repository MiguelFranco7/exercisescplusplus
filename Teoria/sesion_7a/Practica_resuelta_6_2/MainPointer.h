#include "stdafx.h"

class CComp1;
class CComp2;
class CComp3;

class CMainPointer
{
public:
	CMainPointer(int iVal, int iVal1, int iVal2, int iVal3);
	CMainPointer(int iVal);

	~CMainPointer();

private:

	unsigned char m_iVal;
	unsigned int m_iVal2;

	CComp1 *m_pComp1;
	CComp2 *m_pComp2;
	CComp3 *m_pComp3;
};