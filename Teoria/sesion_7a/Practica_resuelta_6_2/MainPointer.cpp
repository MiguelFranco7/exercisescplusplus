#include "stdafx.h"
#include "MainPointer.h"
#include "Comp1.h"
#include "Comp2.h"
#include "Comp3.h"

CMainPointer::CMainPointer(int iVal, int iVal1, int iVal2, int iVal3)
{
	m_pComp3 = new CComp3(iVal3);
	m_pComp2 = new CComp2(iVal2);
	m_pComp1 = new CComp1(iVal1);
	printf("CMainPointer::CMainPointer\n");
}

CMainPointer::CMainPointer(int iVal)
{
	m_pComp3 = new CComp3(iVal);
	m_pComp2 = new CComp2(iVal);
	m_pComp1 = new CComp1(iVal);

	m_iVal = iVal;
}

CMainPointer::~CMainPointer()
{
	delete m_pComp3;
	delete m_pComp2;
	delete m_pComp1;
}