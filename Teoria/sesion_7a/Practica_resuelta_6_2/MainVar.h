#include "stdafx.h"

#include "Comp1.h"
#include "Comp2.h"
#include "Comp3.h"

class CMainVar
{
public:
	CMainVar(int iVal, int iVal1, int iVal2, int iVal3);
	CMainVar(int iVal);

private:
	unsigned char m_iVal;
	unsigned int m_iVal2;

	CComp1 m_oComp1;
	CComp2 m_oComp2;
	CComp3 m_oComp3;
};
