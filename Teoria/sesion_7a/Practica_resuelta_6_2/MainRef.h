#include "stdafx.h"

#include "Comp1.h"
#include "Comp2.h"
#include "Comp3.h"

class CMainRef
{
public:
	CMainRef(int iVal, CComp1 &oComp1, CComp2 &oComp2, CComp3 &oComp3);

	~CMainRef();

private:

	unsigned char m_iVal;
	unsigned int m_iVal2;

	CComp1 &m_oComp1;
	CComp2 &m_oComp2;
	CComp3 &m_oComp3;
};