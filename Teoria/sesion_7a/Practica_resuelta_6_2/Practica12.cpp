// Practica12.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "MainVar.h"
#include "MainRef.h"
#include "MainPointer.h"

// Practica de formas de inicializar variables miembro
/*
class CMiClase
{
public:
		CMiClase(int iVal);
private:
	int m_iVal;
};

CMiClase::CMiClase(int iVal)// : m_iVal(iVal)
{
	m_iVal = iVal;
}
*/

int _tmain(int argc, _TCHAR* argv[])
{
	CMainVar		 oMainVar(1, 2, 3, 4);
	CMainPointer oMainPointer(1, 2, 3, 4);

	CComp1 c1(1);
	CComp2 c2(2);
	CComp3 c3(3);
	CMainRef oMainRef(0, c1, c2, c3);

	printf("sizeof(oMainVar): %d", sizeof(oMainVar));

//	CMiClase oMiClase(0xFFFFFFFF);

	getchar();

	return 0;
}

