namespace NSEjemplo
{
	const int ERROR = 0xFFFFFFFF;

	enum TEnumEjemplo
	{
		EVal0,
		EVal1,
	};

	int	funcionEjemplo(TEnumEjemplo v);

	const int CONST_EJEMPLO = 10;

	extern const char *g_aVariableEjemplo[CONST_EJEMPLO];

}