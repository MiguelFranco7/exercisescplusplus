#include "IFIles.h"

namespace NFileUtils
{
	using namespace IFiles;
	int	GetNumStringTimes	(const char *pszFileName, const char *psz);
	int	SumFileNumbers		(const char *pszFileName);
}
