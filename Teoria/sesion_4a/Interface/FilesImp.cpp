#include "stdafx.h"
#include "IFiles.h"
#include "FilesImp.h"
#include <stdio.h>

FILE * NFilesImp::g_aFiles[NFilesImp::MAX_FILES];

// *****************************************************************
//
// *****************************************************************
int	NFilesImp::OpenFile(const char *pszFileName, IFiles::TFileMode eMode)
{
	int iRet = IFiles::ERROR;
	int i = 0;
	while ((iRet == IFiles::ERROR) && (i < MAX_FILES))
	{
		if (!g_aFiles[i])
			iRet = i;
		else
			i++;
	}

	if (iRet != IFiles::ERROR)
	{
		g_aFiles[iRet] = fopen(pszFileName, eMode == IFiles::EFileMode_Read ? "r" : "w");
		if (!g_aFiles[iRet])
			throw IFiles::EFileException_OpenFile;
			//iRet = IFiles::ERROR;
	}
	return iRet;
}

// *****************************************************************
//
// *****************************************************************
unsigned int NFilesImp::ReadFile	(int iIdFile, unsigned char *pBuffer, unsigned int uNumBytes)
{
	unsigned int uRet = 0;
	if ((iIdFile < MAX_FILES) && (g_aFiles[iIdFile]) && pBuffer)
	{
		int iRet = fread(pBuffer, 1, uNumBytes, g_aFiles[iIdFile]);
		if (iRet > 0)
			uRet = static_cast<unsigned int>(iRet);
		else
			throw IFiles::EFileException_ReadFile;
	}
	return uRet;
}

// *****************************************************************
//
// *****************************************************************
unsigned int NFilesImp::WriteFile(int iIdFile, unsigned char *pBuffer, unsigned int uNumBytes)
{
	unsigned int uRet = 0;
	if ((iIdFile < MAX_FILES) && (g_aFiles[iIdFile]) && pBuffer)
	{
		int iRet = fwrite(pBuffer, 1, uNumBytes, g_aFiles[iIdFile]);
		if (iRet > 0)
			uRet = static_cast<unsigned int>(iRet);
		else
			throw IFiles::EFileException_WriteFile;
	}
	return uRet;
}

// *****************************************************************
//
// *****************************************************************
void NFilesImp::CloseFile(int iIdFile)
{
	if ((iIdFile < MAX_FILES) && (g_aFiles[iIdFile]))
	{
		fclose(g_aFiles[iIdFile]);
		g_aFiles[iIdFile] = NULL;
	}
}
