#include "stdafx.h"
#include "IFiles.h"

namespace NFilesImp
{
	int						OpenFile	(const char *pszFileName, IFiles::TFileMode eMode);
	unsigned int	ReadFile	(int iIdFile, unsigned char *pBuffer, unsigned int uNumBytes);
	unsigned int	WriteFile	(int iIdFile, unsigned char *pBuffer, unsigned int uNumBytes);
	void					CloseFile	(int iIdFile);

	const int MAX_FILES = 10;

	extern FILE * g_aFiles[MAX_FILES];
}