#include "stdafx.h"
#include "Files.h"
#include <stdio.h>

FILE * NFiles::g_aFiles[NFiles::MAX_FILES];

// *****************************************************************
//
// *****************************************************************
int	NFiles::OpenFile(const char *pszFileName, TFileMode eMode)
{
	int iRet = ERROR;
	int i = 0;
	while ((iRet == ERROR) && (i < MAX_FILES))
	{
		if (!g_aFiles[i])
			iRet = i;
		else
			i++;
	}

	if (iRet != ERROR)
	{
		g_aFiles[iRet] = fopen(pszFileName, eMode == EFileMode_Read ? "r" : "w");
		if (!g_aFiles[iRet])
			iRet = ERROR;
	}
	return iRet;
}

// *****************************************************************
//
// *****************************************************************
unsigned int NFiles::ReadFile	(int iIdFile, unsigned char *pBuffer, unsigned int uNumBytes)
{
	unsigned int uRet = 0;
	if ((iIdFile < MAX_FILES) && (g_aFiles[iIdFile]) && pBuffer)
	{
		int iRet = fread(pBuffer, 1, uNumBytes, g_aFiles[iIdFile]);
		if (iRet > 0)
			uRet = static_cast<unsigned int>(iRet);
	}
	return uRet;
}

// *****************************************************************
//
// *****************************************************************
unsigned int NFiles::WriteFile(int iIdFile, unsigned char *pBuffer, unsigned int uNumBytes)
{
	unsigned int uRet = 0;
	if ((iIdFile < MAX_FILES) && (g_aFiles[iIdFile]) && pBuffer)
	{
		int iRet = fwrite(pBuffer, 1, uNumBytes, g_aFiles[iIdFile]);
		if (iRet > 0)
			uRet = static_cast<unsigned int>(iRet);
	}
	return uRet;
}

// *****************************************************************
//
// *****************************************************************
void NFiles::CloseFile(int iIdFile)
{
	if ((iIdFile < MAX_FILES) && (g_aFiles[iIdFile]))
	{
		fclose(g_aFiles[iIdFile]);
		g_aFiles[iIdFile] = NULL;
	}
}
