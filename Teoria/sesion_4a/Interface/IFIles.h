#pragma once
#include "stdafx.h"

namespace IFiles
{
	const int ERROR = 0xFFFFFFFF;

	enum TFileException
	{
		EFileException_OpenFile, 
		EFileException_ReadFile, 
		EFileException_WriteFile
	};

	enum TFileMode
	{
		EFileMode_Read,
		EFileMode_Write,
	};

	int						OpenFile	(const char *pszFileName, TFileMode eMode);
	unsigned int	ReadFile	(int iIdFile, unsigned char *pBuffer, unsigned int uNumBytes);
	unsigned int	WriteFile	(int iIdFile, unsigned char *pBuffer, unsigned int uNumBytes);
	void					CloseFile	(int iIdFile);
}