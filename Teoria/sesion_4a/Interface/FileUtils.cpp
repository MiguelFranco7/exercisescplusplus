#include "stdafx.h"

#include "IFiles.h"
#include "FileUtils.h"
#include <stdlib.h>
#include <string.h>

const int MaxBufferSize = 5;

//using namespace Files;

// ********************************************************************************
//
// ********************************************************************************
int NFileUtils::GetNumStringTimes(const char *pszFileName, const char *psz)
{
	int uRet = 0;
	if (psz && pszFileName)
	{
		int iIdFile = IFiles::OpenFile(pszFileName, IFiles::EFileMode_Read);
		if (iIdFile != IFiles::ERROR)
		{
			char Buffer[MaxBufferSize + 1];
			int iLen = strlen(psz);
			if (iLen <= MaxBufferSize)
			{
        unsigned int uBytesRead = 0;
        try
        { 
          uBytesRead = IFiles::ReadFile(iIdFile, reinterpret_cast<unsigned char *>(Buffer), MaxBufferSize);
        }
        catch (NFileUtils::TFileException e){}
        
				if (uBytesRead)
					Buffer[uBytesRead] = '\0';
				int iDesp = iLen - 1;
				while (uBytesRead > 0)
				{
					const char *pFound = strstr(Buffer, psz);
					while (pFound)
					{
						uRet++;
						pFound++;
						pFound = strstr(pFound, psz);
					}
					for (int i = 0; i < iDesp; i++)
						Buffer[i] = Buffer[MaxBufferSize - iLen + 1 + i];

          try
          {
            uBytesRead = IFiles::ReadFile(iIdFile, reinterpret_cast<unsigned char *>(Buffer + iDesp), MaxBufferSize - iDesp);
          }
          catch (NFileUtils::TFileException e)
          {
            uBytesRead = 0;
          };
					Buffer[iDesp + uBytesRead] = '\0';
				}
			}
			IFiles::CloseFile(iIdFile);
		}
	}
	return uRet;
}

// ********************************************************************************
//
// ********************************************************************************
int	NFileUtils::SumFileNumbers(const char *pszFileName)
{
	int uRet = 0;
	if (pszFileName)
	{
		int iIdFile = IFiles::OpenFile(pszFileName, IFiles::EFileMode_Read);
		if (iIdFile != IFiles::ERROR)
		{
			char Buffer[MaxBufferSize + 1];
      unsigned int uBytesRead = 0;
      try
      {
        uBytesRead = IFiles::ReadFile(iIdFile, reinterpret_cast<unsigned char *>(Buffer), MaxBufferSize);
      }
      catch (NFileUtils::TFileException e){}

			if (uBytesRead)
				Buffer[uBytesRead] = '\0';

			while (uBytesRead > 0)
			{
				const char	*pIni		= Buffer;
				char				*pFound = strstr(Buffer, ",");
				while (pFound)
				{
					*pFound = '\0';
					uRet += atoi(pIni);
					pFound++;
					pIni = pFound;
					pFound = strstr(pFound, ",");
				}

				int iCount = (Buffer + MaxBufferSize) - pIni;
				for (int i = 0; i < iCount; i++)
					Buffer[i] = pIni[i];

        try
        {
          uBytesRead = IFiles::ReadFile(iIdFile, reinterpret_cast<unsigned char *>(Buffer + iCount), MaxBufferSize - iCount);
        }
        catch (NFileUtils::TFileException e)
        {
          uBytesRead = 0;
        }
				
				Buffer[iCount + uBytesRead] = '\0';
			}

			uRet += atoi(Buffer);

			IFiles::CloseFile(iIdFile);
		}
	}
	return uRet;
}