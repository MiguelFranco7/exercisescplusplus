#include "stdafx.h"
#include "IFIles.h"
#include "FilesImp.h"


int	IFiles::OpenFile(const char *pszFileName, TFileMode eMode)
{
	return NFilesImp::OpenFile(pszFileName, eMode);
}

unsigned int IFiles::ReadFile	(int iIdFile, unsigned char *pBuffer, unsigned int uNumBytes)
{
	return NFilesImp::ReadFile(iIdFile, pBuffer, uNumBytes);
}

unsigned int IFiles::WriteFile(int iIdFile, unsigned char *pBuffer, unsigned int uNumBytes)
{
	return NFilesImp::WriteFile(iIdFile, pBuffer, uNumBytes);
}

void IFiles::CloseFile (int iIdFile)
{
	return NFilesImp::CloseFile(iIdFile);
}