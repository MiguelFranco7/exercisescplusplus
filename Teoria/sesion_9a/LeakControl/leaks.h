#ifndef _____LEAKS_CONTROL____
#define _____LEAKS_CONTROL____
#ifdef _DEBUG
#pragma warning(disable: 4100)
#pragma warning(disable: 4996)
#include <stdlib.h>

struct ALLOC_INFO 
{
	  unsigned int	address;
	  unsigned int	size;
	  char	file[_MAX_PATH];
	  unsigned int	line;
		ALLOC_INFO *pNext;
};

void RemoveTrack(unsigned int addr);
void AddTrack(unsigned int addr,  unsigned int asize,  const char *fname, unsigned int lnum);

inline void * __cdecl operator new(unsigned int  size, const char *file, int line)
{
	void *ptr = (void *)malloc(size);
	AddTrack((unsigned int)ptr, size, file, line);
	return(ptr);
};

inline void __cdecl operator delete(void *p)
{
	RemoveTrack((unsigned int)p);
	free(p);
};

inline void * __cdecl operator new[](unsigned int size, const char *file, int line)
{
	void *ptr = (void *)malloc(size);
	AddTrack((unsigned int)ptr, size, file, line);
	return(ptr);
};

inline void __cdecl operator delete[](void *p)
{
	RemoveTrack((unsigned int)p);
	free(p);
}

void DumpUnfreed();

extern ALLOC_INFO *g_LeakList;

#define NEW new(__FILE__, __LINE__) 

#endif

#endif