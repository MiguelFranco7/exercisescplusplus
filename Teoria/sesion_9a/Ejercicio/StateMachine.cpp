#include "stdafx.h"
#include "StateMachine.h"

// Tabla de transiciones
CStateMachine::TStateInfo CStateMachine::m_aTransitions[] = 
{
#define REG_STATE(StateName, StateFunc) \
	{ StateName, &StateFunc},
#include "RegStates.h"
#undef REG_STATE

	/*
	{TEvent_Event1, &StateFunc1},
	{TEvent_Event2, &StateFunc2},
	{TEvent_Event3, &StateFunc3},
	*/
};

const char *GetEventName(CStateMachine::TEvent e);

// *********************************************************************
//
// *********************************************************************
CStateMachine::CStateMachine()
{
	m_funcCurrState = &CStateMachine::StateFunc1;
}

// *********************************************************************
//
// *********************************************************************
void CStateMachine::EjecutarEvento(TEvent eEvent)
{
	if (m_funcCurrState)
		(this->*m_funcCurrState)(eEvent);
}

// *********************************************************************
//
// *********************************************************************
void CStateMachine::StateFunc1(TEvent e)
{
	const char *pszNewStateName = GetEventName(e);
	printf("CStateMachine::StateFunc1(TEvent e): Event:%s\n", pszNewStateName);
	m_funcCurrState = m_aTransitions[e].pFunc;
}

// *********************************************************************
//
// *********************************************************************
void CStateMachine::StateFunc2(TEvent e)
{
	const char *pszNewStateName = GetEventName(e);
	printf("CStateMachine::StateFunc2(TEvent e): Event:%s\n", pszNewStateName);
	m_funcCurrState = m_aTransitions[e].pFunc;
}

// *********************************************************************
//
// *********************************************************************
void CStateMachine::StateFunc3(TEvent e)
{
	const char *pszNewStateName = GetEventName(e);
	printf("CStateMachine::StateFunc3(TEvent e): Event:%s\n", pszNewStateName);
	m_funcCurrState = m_aTransitions[e].pFunc;
}

// *********************************************************************
//
// *********************************************************************
const char *GetEventName(CStateMachine::TEvent e)
{
	#define REG_STATE(StateName, StateFunc) \
		case CStateMachine::StateName: return #StateName;
		
	switch (e)
	{
#include "RegStates.h"
	}
#undef REG_STATE
}