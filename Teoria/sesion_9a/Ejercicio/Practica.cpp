// Practica19.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <string>
#include <Windows.h>

#include "StateMachine.h"

int _tmain(int argc, _TCHAR* argv[])
{
	char MsgBuffer[512];
	sprintf(MsgBuffer, "%s(%d): Estoy AQUI\n", __FILE__, __LINE__);

	OutputDebugString(MsgBuffer);


	//State machine
	CStateMachine oStateMachine;

	oStateMachine.EjecutarEvento(CStateMachine::TEvent_Event2);
	oStateMachine.EjecutarEvento(CStateMachine::TEvent_Event3);
	oStateMachine.EjecutarEvento(CStateMachine::TEvent_Event1);

	getchar();

	return 0;
}

