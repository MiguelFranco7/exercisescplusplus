#include "stdafx.h"

class CStateMachine
{
public:
	CStateMachine();

	enum TEvent 
	{
#define REG_STATE(StateName, StateFunc) \
	  StateName,
#include "RegStates.h"
#undef REG_STATE
		/*
		TEvent_Event1,
		TEvent_Event2,
		TEvent_Event3,
		*/
	};

	void EjecutarEvento(TEvent eEvent);

protected:
	void StateFunc1(TEvent e);
	void StateFunc2(TEvent e);
	void StateFunc3(TEvent e);

	typedef void (CStateMachine::*StateFuncPounter)(TEvent);

	struct TStateInfo
	{
		TEvent						eEvent;
		StateFuncPounter	pFunc;
	};

	static TStateInfo m_aTransitions[];

	StateFuncPounter m_funcCurrState;
};
