// Practica.cpp
//
#include <stdio.h>
#include <cstdarg>

typedef int(*TypeFunc)(char, char);

int f1(char c1, char c2);
//char f1(char c1, char c2);
//int f1(int c1, int c2);
int f1(unsigned int c1, unsigned int c2);

int f2(char c1, char c2);
int f3(char c1, char c2);
TypeFunc GetFunction(int idx);

 int (*GetFunction2(int idx)) (char, char);

// Tablas de funciones.
TypeFunc TablaFunc[3] = {&f1, &f2, &f3};
//int (*TablaFunc[3])(char, char) = {&f1, &f2, &f3};

void MyPrintf(const char *format ...);

int main(int argc, char* argv[])
{
	argc;
	argv;

	// Sobrecarga
	f1((char)1, (char)3);

	// Punteros a funciones;
	int (*p)(char, char) = &f1;
	
	TypeFunc p2 = &f1;
	TypeFunc p3 = f1;

	p(1, 2);
	(*p)(2, 3);

	(*p2)(2, 3);

	p3(5, 6);

	TypeFunc p4 = GetFunction2(2);
	p4(7, 8);

	MyPrintf("U-TAD: %s%d", "Sesion", 5);

	getchar();

	return 0;
}

int f1(char c1, char c2)
{
	return (c1 + c2);
}

int f2(char c1, char c2)
{
	return (c1 * c2);
}

int f3(char c1, char c2)
{
	return (c1 / c2);
}

/*
int f1(int c1, int c2)
{
	return (c1 + c2);
}
*/

int f1(unsigned int c1, unsigned int c2)
{
	return (c1 + c2);
}

void MyPrintf(const char *format ...)
{
	va_list ParamList;
	va_start(ParamList, format);
	vprintf(format, ParamList);
	va_end(ParamList);
}

TypeFunc GetFunction(int idx)
{
	return TablaFunc[idx];
}

int (*GetFunction2(int idx)) (char, char)
{
	return TablaFunc[idx];
}