// Practica.cpp : Defines the entry point for the console application.
//
#include <string.h>
#include <stdio.h>

#pragma warning(disable:4996)

int Suma(int i1, int i2);
int nada(int foo);
int Fact(int i);
int Fact2(int i);

// ***************************************
// Tabla de cadenas
// ***************************************
const char *g_Strings[] = 
{
	"",
	"Hola",
	"Adios",
};

const int g_NumStrings = sizeof(g_Strings) / sizeof(*g_Strings);


// ***************************************
// 
// ***************************************
bool GetSpanishString(int iId, const char *&pString)
{
	bool bRet = false;
	if (iId < g_NumStrings)
	{
		pString = g_Strings[iId];
		bRet = true;
	}

	return bRet;
}

// ***************************************
// 
// ***************************************
bool GetSpanishStringV2(int iId, char *pString)
{
	bool bRet = false;
	if ((iId < g_NumStrings) && pString)
	{
		strcpy(pString, g_Strings[iId]);
		bRet = true;
	}

	return bRet;
}

// ***************************************
// 
// ***************************************
bool GetSpanishStringV3(int iId, const char **ppString)
{
	bool bRet = false;
	if (iId < g_NumStrings)
	{
		bRet = true;
		if (ppString)
			*ppString = g_Strings[iId];		
	}

	return bRet;
}

// ***********************************************************
typedef unsigned char uint8;

int main(int argc, char* argv[])
{
	// Llamada a funciones.
	nada(7);
	int i = Suma(3, 5);

	int f  = Fact(5);
	int f2 = Fact2(5);

	// Paso de parámetros a funciones.
	const char *pString = NULL;
	if (GetSpanishString(1, pString))
		printf("%s\n", pString);

	char szString[256];
	if (GetSpanishStringV2(2, szString))
		printf("%s\n", szString);

	//GetSpanishString(1, szString)
	//GetSpanishStringV2(2, pString)

	getchar();

	return 0;
}

// *********************************************************************
int nada(int foo)
{
  int local1 = foo;
  int local2 = foo;
  int local3 = foo;
	return local1;
}

int Suma(int i1, int i2)
{
	int a1 = 5;
	int a2 = 4;
	return (i1 * a1 + i2 * a2);
}

int Fact(int i)
{
	int ret = 1;
	while (i)
		ret *= i--;
	return ret;
}

int Fact2(int i)
{
	__asm
	{
		mov eax, 1
		mov ecx, [i]
	bucle:
		imul eax,ecx
		dec ecx
		jnz bucle
	}
}
