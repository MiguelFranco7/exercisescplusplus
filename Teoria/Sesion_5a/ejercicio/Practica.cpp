// Practica9.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "MiClase.h"

// *******************************************************
void BadFunction() {
	throw 10;
}

// *******************************************************
struct TLista {
	TLista() {
		m_uMaxSize = 1000;
	}

	//explicit TLista(unsigned int uMaxSize) 
	TLista(unsigned int uMaxSize) {
		m_uMaxSize = uMaxSize;
		//	BadFunction();
	}

	TLista(const TLista& lista) {
		m_uMaxSize = lista.m_uMaxSize;
	}

	~TLista() {
		m_uMaxSize = 0;
	}

private:
	unsigned int m_uMaxSize;
};

// *******************************************************
void foo(TLista) {
}

// *******************************************************
int Init() {
	int i = 0xFFFFFFFF;
	return i;
}

// *******************************************************
class CCuantoOcupaEstaClase1 {
public:
	char m_c;
	int  m_i;
};

// *******************************************************
class CCuantoOcupaEstaClase2 {
public:
	char m_c1;
	char m_c2;
	int  m_i;
};

// *******************************************************
class CCuantoOcupaEstaClase3 {
public:
	char m_c1;
	char m_c2;
	int  m_i;
	char m_c3;
};

int main(int argc, _TCHAR* argv[]) {
	int iSize1 = sizeof(CCuantoOcupaEstaClase1);
	int iSize2 = sizeof(CCuantoOcupaEstaClase2);
	int iSize3 = sizeof(CCuantoOcupaEstaClase3);

	CMiClase miClase;

	Init();
	miClase.Init();

	try {
		// Constructor implícito.
		TLista lista = 100;
		foo(200);

		// Constructor de copia.
		TLista lista2 = lista;
	} catch (int e) {};

	TLista *pTablaList = new TLista[5];
	delete[]pTablaList;

	return 0;
}

