#include "stdafx.h"

class CMiClaseMiembro {
public:
	CMiClaseMiembro() { m_i = 0; }
	int m_i;
};

class CMiClase {
public:
	int Init() {
		m_i = 0xFFFFFFFF;
		return m_i;
	}

	CMiClaseMiembro m_oMiClaseMiembro;
	int							m_i;
};