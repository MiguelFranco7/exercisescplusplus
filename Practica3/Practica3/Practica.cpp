#include <string.h>
#include <stdio.h>
#include <iostream>

#pragma warning(disable:4996)

// ***************************************
// Tabla de cadenas
// ***************************************
const char *g_Strings[] = {
	"",
	"Hola",
	"Adios",
};

char tablaResult[256];

const int g_NumStrings = sizeof(g_Strings) / sizeof(*g_Strings);

const char * getStringById(int iId) {
	const char *p = nullptr;

	if (iId < g_NumStrings)
	{
		p = g_Strings[iId];
	}

	return p;
}

const char * getInvertedStringById(int iId) {
	const char *p = nullptr;

	if (iId < g_NumStrings) {
		const char *p1 = g_Strings[iId];
		unsigned int i = 0;
		unsigned int j = strlen(g_Strings[iId]) - 1;

		while (i <= j) {
			tablaResult[i] = *(p1 + j);
			tablaResult[j] = *(p1 + i);
			i++;
			j--;
		}

		p = tablaResult;
	}

	return p;
}

typedef unsigned char uint8;

int main(int argc, char* argv[]) {

	const char *p = getStringById(2);
	const char *p1 = getInvertedStringById(2);

	std::cout << p << std::endl;
	std::cout << p1;

	getchar();

	return 0;
}