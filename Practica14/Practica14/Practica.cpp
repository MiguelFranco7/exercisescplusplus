#include <vector>
#include "stdafx.h"

#include "Jpg.h"
#include "Png.h"

void PrintImageInfo(const CImagen &oImagen);
void RemoveAllAlpha(std::vector<CImagen *> images);

int main() {
	CPng *oPng  = new CPng;
	CJpg *oJpg  = new CJpg;
	CPng *oPng1 = new CPng;
	CJpg *oJpg1 = new CJpg;

	oPng->SetPngFile("Fichero.png");
	oJpg->SetJpgFile("Fichero.jpg");
	oPng1->SetPngFile("Fichero1.png");
	oJpg1->SetJpgFile("Fichero1.jpg");

	PrintImageInfo(*oPng);
	PrintImageInfo(*oJpg);
	PrintImageInfo(*oPng1);
	PrintImageInfo(*oJpg1);

	std::vector<CImagen *> images;
	images.push_back(oPng);
	images.push_back(oJpg);
	images.push_back(oPng1);
	images.push_back(oJpg1);

	RemoveAllAlpha(images);

	getchar();

	return 0;
}

void PrintImageInfo(const CImagen &oImagen) {
	printf("NumPixes: %d \nSize: %d\n", oImagen.GetNumPixels(), oImagen.GetUncompressedSize());
}

void RemoveAllAlpha(const std::vector<CImagen *> images) {
	for (int i = 0; i < images.size(); i++) {
		if (images[i]->GetImageType() == CImagen::EPng) {
			CPng *png = static_cast<CPng *>(images[i]);
			png->RemoveAlphaChannel();
		}
	}
}