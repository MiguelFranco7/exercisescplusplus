#include "stdafx.h"
#include "Png.h"

CPng::CPng() {
	m_uImageType = EPng;
}

int CPng::SetPngFile(const char *pszPngFile) {
	printf("Lectura del fichero comprimido Png\n");
	UncompressImage();
	return 0;
}

int CPng::RemoveAlphaChannel() {
	printf("Eliminando canal alpha de Imagen Png\n");
	return 0;
}

int CPng::UncompressImage() {
	printf("Descompresion de Imagen Png\n");
	m_uResX = 480;
	m_uResY = 320;
	m_uColorBytes = 4;
	m_uSize = m_uResX * m_uResY * m_uColorBytes;
	m_pBuffer = new unsigned char[m_uSize];
	strcpy(reinterpret_cast<char *>(m_pBuffer), "Bytes del buffer descomprimido desde Png");
	return 0;
}