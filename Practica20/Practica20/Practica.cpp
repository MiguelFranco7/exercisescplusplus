#include <stdio.h>
#include <list>
#include <vector>
#include "consola.h"

struct TEntity;
typedef void(*funcEntity)(TEntity *);

void generateNewEntity();

// ***************************************************************************************
// ENTIDAD
// ***************************************************************************************
struct TEntity {
	int m_ix;
	int m_iy;
	int m_live;
	funcEntity *m_funcs;

	TEntity(funcEntity *funcs, int x, int y, int live) {
		m_ix = x;
		m_iy = y;
		m_funcs = funcs;
		m_live = live;
	}

	void insertFunc(funcEntity f, int pos) { m_funcs[pos] = f; }
};

void move(TEntity * e) {
	e->m_ix++;
	e->m_iy++;
}

void print(TEntity * e) {
	gotoxy(static_cast<short>(e->m_ix), static_cast<short>(e->m_iy));
	printf("%c", '+');
}

void move1(TEntity * e) {
	e->m_ix += 2;
	e->m_iy++;
}

void print1(TEntity * e) {
	gotoxy(static_cast<short>(e->m_ix), static_cast<short>(e->m_iy));
	printf("%c", '%');
}

void move2(TEntity * e) {
	e->m_ix++;
	e->m_iy += 2;
}

void print2(TEntity * e) {
	gotoxy(static_cast<short>(e->m_ix), static_cast<short>(e->m_iy));
	printf("%c", '$');
}

void move3(TEntity * e) {
	e->m_ix += 3;
	e->m_iy += 2;
}

void print3(TEntity * e) {
	gotoxy(static_cast<short>(e->m_ix), static_cast<short>(e->m_iy));
	printf("%c", '@');
}

std::vector<funcEntity> listMove;
std::vector<funcEntity> listPrint;
std::list<TEntity *> list;

unsigned int uKey;

int main(int argc, char* argv[]) {
	listMove.push_back(&move);
	listMove.push_back(&move1);
	listMove.push_back(&move2);
	listMove.push_back(&move3);

	listPrint.push_back(&print);
	listPrint.push_back(&print1);
	listPrint.push_back(&print2);
	listPrint.push_back(&print3);

	while (true) {
		Sleep(500);
		hidecursor();
		clear();

		if (list.size() < 5) {
			generateNewEntity();
		}

		for (auto it = list.begin(); it != list.end(); ++it) {
			(*it)->m_funcs[0](*it);
			(*it)->m_funcs[1](*it);
			(*it)->m_live--;

			if ((*it)->m_live <= 0) {
				delete *it;
				it = list.erase(it);
				if (it == list.end()) break;
			}
		}
	}
}

void generateNewEntity() {
	funcEntity *func1 = new funcEntity();
	func1[0] = listMove[rand() % 4];
	func1[1] = listPrint[rand() % 4];

	TEntity * ent = new TEntity(func1, rand() % 10, rand() % 10, rand() % 20);

	list.push_back(ent);
}