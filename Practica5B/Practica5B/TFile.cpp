#define _CRT_SECURE_NO_DEPRECATE
#include "TFile.h"

const int ERROR = 0xFFFFFFFF;
const int MAX_FILES = 10;
FILE * g_aFiles[MAX_FILES];

int	OpenFile(const char *pszFileName, TFileMode eMode) {
	int iRet = ERROR;

	if (pszFileName != nullptr) {
		int i = 0;
		while ((iRet == ERROR) && (i < MAX_FILES)) {
			if (!g_aFiles[i])
				iRet = i;
			else
				i++;
		}

		if (iRet != ERROR) {
			g_aFiles[iRet] = fopen(pszFileName, eMode == EFileMode_Read ? "r" : "w");
			if (!g_aFiles[iRet])
				iRet = ERROR;
		}
	}

	return iRet;
}

unsigned int ReadFile(int iIdFile, unsigned char *pBuffer, unsigned int uNumBytes) {
	unsigned int uRet = 0;

	if (pBuffer != nullptr && iIdFile < MAX_FILES && iIdFile >= 0) {
		if ((iIdFile < MAX_FILES) && (g_aFiles[iIdFile]) && pBuffer) {

			uRet = fread(pBuffer, 1, uNumBytes, g_aFiles[iIdFile]);
		}
	}

	return uRet;
}

unsigned int WriteFile(int iIdFile, const char *pBuffer, unsigned int uNumBytes) {
	unsigned int uRet = 0;

	if (pBuffer != nullptr && iIdFile < MAX_FILES && iIdFile >= 0 && uNumBytes != NULL) {
		if ((iIdFile < MAX_FILES) && (g_aFiles[iIdFile]) && pBuffer) {
			unsigned int iRet = fwrite(pBuffer, sizeof(char), uNumBytes, g_aFiles[iIdFile]);
			if (iRet > 0)
				uRet = static_cast<unsigned int>(iRet);
		}
	}

	return uRet;
}

void CloseFile(int iIdFile) {
	if ((iIdFile < MAX_FILES && iIdFile >= 0) && (g_aFiles[iIdFile])) {
		fclose(g_aFiles[iIdFile]);
		g_aFiles[iIdFile] = NULL;
	}
}

unsigned int fileSize(int idFile) {
	if (idFile < MAX_FILES && idFile >= 0) {
		fseek(g_aFiles[idFile], 0, SEEK_END);
		unsigned int size = static_cast<unsigned int>(ftell(g_aFiles[idFile]));
		rewind(g_aFiles[idFile]);

		return size;
	} else return 0;
}