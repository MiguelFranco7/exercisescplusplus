#include <iostream>
#include <cstdio>
#include "TFile.h"

using namespace std;

int main(int argc, char* argv[]) {

	int id = OpenFile("Prueba.txt", EFileMode_Read);

	unsigned int lSize = fileSize(id);
	unsigned char *buffer = new unsigned char[lSize];
	buffer[lSize] = '\0';

	unsigned int numCar = ReadFile(id, buffer, lSize);
	CloseFile(id);

	cout << "NumCar: " << numCar << endl << buffer << endl;

	int id1 = OpenFile("Prueba.txt", EFileMode_Write);
	const char *buffer1 = "Holaa Miguel";
	numCar = WriteFile(id, buffer1, strlen(buffer1));
	CloseFile(id1);

	cout << "NumCar: " << numCar << endl << buffer1 << endl;

	getchar();

	return 0;
}