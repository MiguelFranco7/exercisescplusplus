// Practica 17

#include "stdafx.h"

class Alert {
public:
	virtual void notify(const char *text) const = 0;
};

class AlertDerived : public Alert {
public:
	void notify(const char *text) const { std::cout << text << std::endl; }
};

class CStream {
public:
	virtual void StreamOpen()  = 0;
	virtual void StreamClose() = 0;
	virtual void StreamRead()  = 0;
	virtual void StreamWrite() = 0;

	virtual void StreamReadAsin(const AlertDerived &alert) = 0;
};

class CFile : public CStream {
public:
	CFile() {
		printf("CFile()\n");
	}

	~CFile() {
		printf("~CFile()\n");
	}

	virtual void StreamOpen() {
		printf("CFile open.\n");
	}

	virtual void StreamClose() {
		printf("CFile close.\n");
	}

	virtual void StreamRead() {
		printf("CFile read.\n");
	}

	virtual void StreamWrite() {
		printf("CFile write.\n");
	}

	virtual void StreamReadAsin(const AlertDerived &alert) {
		printf("CFile Read Asin.\n");
		Sleep(2000);
		alert.notify("CFile.");
	}
};

class CCom : public CStream {
public:
	CCom() {
		printf("CCom()\n");
	}

	~CCom() {
		printf("~CCom()\n");
	}

	virtual void StreamOpen() {
		printf("CCom open.\n");
	}

	virtual void StreamClose() {
		printf("CCom close.\n");
	}

	virtual void StreamRead() {
		printf("CCom read.\n");
	}

	virtual void StreamWrite() {
		printf("CCom write.\n");
	}

	virtual void StreamReadAsin(const AlertDerived &alert) {
		printf("CCom Read Asin.\n");
		Sleep(2000);
		alert.notify("CCom.");
	}
};

class CTcp : public CStream {
public:
	CTcp() {
		printf("CTcp()\n");
	}

	~CTcp() {
		printf("~CTcp()\n");
	}

	virtual void StreamOpen() {
		printf("CTcp open.\n");
	}

	virtual void StreamClose() {
		printf("CTcp close.\n");
	}

	virtual void StreamRead() {
		printf("CTcp read.\n");
	}

	virtual void StreamWrite() {
		printf("CTcp write.\n");
	}

	virtual void StreamReadAsin(const AlertDerived &alert) {
		printf("CTcp Read Asin.\n");
		Sleep(2000);
		alert.notify("CTcp.");
	}
};

int _tmain(int argc, _TCHAR* argv[]) {
	//CStream *pBase = new CStream();
	CStream *pStreamF = new CFile();
	CStream *pStreamC = new CCom();
	CStream *pStreamT = new CTcp();
	CFile *pFile = new CFile();
	CCom *pCom = new CCom();
	CTcp *pTcp = new CTcp();
	AlertDerived *alert = new AlertDerived();

	// Llamada a m�todos virtuales
	pStreamF->StreamOpen();
	pStreamC->StreamOpen();
	pStreamT->StreamOpen();
	/*pFile->StreamRead();
	pCom->StreamRead();
	pTcp->StreamRead();*/
	pFile->StreamReadAsin(*alert);
	pCom->StreamReadAsin(*alert);
	pTcp->StreamReadAsin(*alert);

	// Borrado desde la clase base. Destructores virtuales
	delete pStreamF;
	delete pStreamC;
	delete pStreamT;
	delete pFile;
	delete pCom;
	delete pTcp;
	delete alert;

	getchar();

	return 0;
}

