#include "TList.h"
#include <cstdio>
#include <stdio.h>

void TList::Init() {
	this->count = 0;
	this->headNode = nullptr;
}

int TList::size() {
	return this->count;
}

int TList::push(const char *p) {
	Node *pNew = new Node;
	Node *pPre = nullptr;
	Node *pCur = this->headNode;
	int len = 0;

	while (*(p + len) != NULL) {
		len++;
	}

	pNew->data = new char[len+1];
	for (int i = 0; i < len; i++) {
		pNew->data[i] = *(p + i);
	}

	pNew->data[len] = '\0';

	while (pCur) {
		pPre = pCur;
		pCur = pCur->next;
	}

	if (pPre) {
		pNew->next = pPre->next;
		pPre->next = pNew;
		this->count++;
	} else {
		pNew->next = this->headNode;
		this->headNode = pNew;
		this->count++;
	}

	return this->count;
}

const char * TList::first() {
	return this->headNode->data;
}

const char * TList::next() {
	return this->headNode->next->data;
}

const char * TList::pop() {
	Node *temp;

	temp = this->headNode;
	this->headNode = this->headNode->next;
	this->count--;

	return temp->data;
}

const char * TList::at(int pos) {
	Node *node = this->headNode;

	for (int i = 0; i < pos; i++) {
		node = node->next;
	}

	return node->data;
}

void TList::reset() {
	char *p;

	while (this->headNode) {
		p = this->headNode->data;
		this->headNode = this->headNode->next;
		delete[]p;
		this->count--;
	}
}

TList::~TList() {
	this->reset();
}