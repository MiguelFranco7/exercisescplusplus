#include <cstdio>

namespace NSFile {
	const int ERROR = 0xFFFFFFFF;

	enum TFileMode
	{
		EFileMode_Read,
		EFileMode_Write
	};

	int OpenFile(const char *pszFileName, TFileMode eMode);
	unsigned int ReadFile(int iIdFile, unsigned char *pBuffer, unsigned int uNumBytes);
	unsigned int WriteFile(int iIdFile, const char *pBuffer, unsigned int uNumBytes);
	void CloseFile(int iIdFile);
	unsigned int FileSize(int idFile);

	//const int MAX_FILES = 10;
	//extern FILE * g_aFiles[MAX_FILES];
}
