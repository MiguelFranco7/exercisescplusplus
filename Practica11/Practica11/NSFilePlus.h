#include "TList.h"

namespace NSFilePlus {

	enum TFileException	{
		EFileException_OpenFile,
		EFileException_ReadFile,
		EFileException_WriteFile
	};

	int	GetNumStringTimes(const char *pszFileName, const char *psz);
	int	SumFileNumbers(const char *pszFileName);
	void GetListNumbers(const char *pszFileName, TList &list);
}
