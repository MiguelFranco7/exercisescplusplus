struct  Node {
	char *data;
	Node *next;
};

class TList {
public:
	void		Init();
	int         size  ();
	int         push  (const char*);
	const char *first ();
	const char *next  ();
	const char *pop   ();
	const char *at    (int pos);
	void        reset ();
	           ~TList ();

private:
	int   count;
	Node *headNode;
};