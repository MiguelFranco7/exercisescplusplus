enum TFileException
{
	EFileException_OpenFile,
	EFileException_ReadFile,
	EFileException_WriteFile
};

int	GetNumStringTimes(const char *pszFileName, const char *psz);
int	SumFileNumbers(const char *pszFileName);
int	printIncludes(const char *pszFileName);
