#include <stdio.h>
#include "consola.h"

struct TEntity;
typedef void(*funcEntity)(TEntity *);

// ***************************************************************************************
// ENTIDAD
// ***************************************************************************************
struct TEntity {
	int m_ix;
	int m_iy;
	funcEntity *m_funcs;
	TEntity(funcEntity *funcs, int x, int y) {
		m_ix = x;
		m_iy = y;
		m_funcs = funcs;
	}
};

void move(TEntity * e) {
	e->m_ix++;
	e->m_iy++;
}

void print(TEntity * e) {
	gotoxy(static_cast<short>(e->m_ix), static_cast<short>(e->m_iy));
	printf("%c", '+');
}

void move1(TEntity * e) {
	e->m_ix += 2;
	e->m_iy++;
}

void print1(TEntity * e) {
	gotoxy(static_cast<short>(e->m_ix), static_cast<short>(e->m_iy));
	printf("%c", '%');
}

void move2(TEntity * e) {
	e->m_ix++;
	e->m_iy += 2;
}

void print2(TEntity * e) {
	gotoxy(static_cast<short>(e->m_ix), static_cast<short>(e->m_iy));
	printf("%c", '$');
}

void move3(TEntity * e) {
	e->m_ix += 3;
	e->m_iy += 2;
}

void print3(TEntity * e) {
	gotoxy(static_cast<short>(e->m_ix), static_cast<short>(e->m_iy));
	printf("%c", '@');
}

unsigned int uKey;

int main(int argc, char* argv[]) {
	TEntity * t_e[4];
	char tabla[4];
	funcEntity func[2] = { &move, &print };
	TEntity * ent = new TEntity(func, 0, 0);

	funcEntity func1[2] = { &move1, &print1 };
	TEntity * ent1 = new TEntity(func1, 2, 2);

	funcEntity func2[2] = { &move2, &print2 };
	TEntity * ent2 = new TEntity(func2, 4, 1);

	funcEntity func3[2] = { &move3, &print3 };
	TEntity * ent3 = new TEntity(func3, 1, 3);

	t_e[0] = ent;
	t_e[1] = ent1;
	t_e[2] = ent2;
	t_e[3] = ent3;

	while (true) {
		Sleep(500);
		hidecursor();
		clear();

		for (int i = 0; i < sizeof(tabla); i++) {
			t_e[i]->m_funcs[0](t_e[i]);
			t_e[i]->m_funcs[1](t_e[i]);
		}
	}

	delete t_e[0];
	delete t_e[1];
	delete t_e[2];
	delete t_e[3];
}