#define _CRT_SECURE_NO_DEPRECATE
#include "TFile.h"

TFile::TFile() {
	file = nullptr;
}

void TFile::OpenFile(const char *pszFileName, TFile::TFileMode eMode) {
	if (file) {
		CloseFile();
	}

	if (pszFileName)
		file = fopen(pszFileName, eMode == EFileMode_Read ? "r" : "w");
}

unsigned int TFile::ReadFile(unsigned char *pBuffer, unsigned int uNumBytes) {
	unsigned int uRet = 0;

	if (file && pBuffer) {

		uRet = fread(pBuffer, 1, uNumBytes, file);
	}

	return uRet;
}

unsigned int TFile::WriteFile(const char *pBuffer, unsigned int uNumBytes) {
	unsigned int uRet = 0;

	if (file && pBuffer) {
		unsigned int iRet = fwrite(pBuffer, sizeof(char), uNumBytes, file);
		if (iRet > 0)
			uRet = static_cast<unsigned int>(iRet);
	}

	return uRet;
}

void TFile::CloseFile() {
	if (file) {
		fclose(file);
		file = nullptr;
	}
}

unsigned int TFile::FileSize() {
	unsigned int size = 0;

	if (file) {
		fseek(file, 0, SEEK_END);
		size = static_cast<unsigned int>(ftell(file));
		rewind(file);
	}

	return size;
}

TFile::~TFile() {
	delete file;
}