#include <cstdio>

class TFile {
public:
	const int ERROR = 0xFFFFFFFF;

	enum TFileMode
	{
		EFileMode_Read,
		EFileMode_Write
	};

				 TFile	   ();
	void		 OpenFile  (const char *pszFileName, TFileMode eMode);
	unsigned int ReadFile  (unsigned char *pBuffer, unsigned int uNumBytes);
	unsigned int WriteFile (const char *pBuffer, unsigned int uNumBytes);
	void		 CloseFile ();
	unsigned int FileSize  ();
				~TFile     ();
	
private:
	FILE * file;
};
