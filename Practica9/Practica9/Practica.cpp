#include <iostream>
#include <cstdio>
#include "TFile.h"

using namespace std;

int main(int argc, char* argv[]) {

	TFile *file = new TFile;

	file->OpenFile("Prueba.txt", file->EFileMode_Read);

	unsigned int lSize = file->FileSize();
	unsigned char *buffer = new unsigned char[lSize];
	buffer[lSize] = '\0';

	unsigned int numCar = file->ReadFile(buffer, static_cast<unsigned int>(lSize));
	file->CloseFile();

	cout << "NumCar: " << numCar << endl << buffer << endl;

	file->OpenFile("Prueba.txt", file->EFileMode_Write);
	const char *buffer1 = "Holaa Miguel";
	numCar = file->WriteFile(buffer1, strlen(buffer1));
	file->CloseFile();

	cout << "NumCar: " << numCar << endl << buffer1 << endl;

	delete file;

	getchar();

	return 0;
}