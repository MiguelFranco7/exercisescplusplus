#include "TList.h"
#include <cstdio>
#include <stdio.h>

void TList::Init() {
	this->count = 0;
	this->headNode = nullptr;
}

TList::TList() {
	printf("TList\n");
	Init();
}

TList::TList(const TList &other) {
	printf("TList copy\n");
	Init();

	Node *node = other.headNode;

	for (int i = 0; i < other.count; i++) {
		this->push(node->data);
		node = node->next;
	}
}

int TList::size() const {
	return this->count;
}

int TList::push(const char *p) {
	Node *pNew = new Node;
	Node *pPre = nullptr;
	Node *pCur = this->headNode;
	int len = 0;

	while (*(p + len) != NULL) {
		len++;
	}

	pNew->data = new char[len + 1];
	for (int i = 0; i < len; i++) {
		pNew->data[i] = *(p + i);
	}

	pNew->data[len] = '\0';

	while (pCur) {
		pPre = pCur;
		pCur = pCur->next;
	}

	if (pPre) {
		pNew->next = pPre->next;
		pPre->next = pNew;
		this->count++;
	}
	else {
		pNew->next = this->headNode;
		this->headNode = pNew;
		this->count++;
	}

	return this->count;
}

const char * TList::first() const {
	if (this->count > 0)
		return this->headNode->data;
	else
		return nullptr;
}

const char * TList::next() const {
	if (this->count > 0)
		return this->headNode->next->data;
	else
		return nullptr;
}

const char * TList::pop() {
	if (this->count > 0) {
		const char *data = this->headNode->data;
		Node *temp = this->headNode;

		this->headNode = this->headNode->next;
		this->count--;
		delete temp;

		return data;
	}
	else
		return nullptr;
}

void TList::reset() {
	char *p;
	Node *n;

	while (this->headNode) {
		p = this->headNode->data;
		n = this->headNode;
		delete p;

		this->headNode = this->headNode->next;
		delete n;
		this->count--;
	}
}

const char * TList::at(int pos) const {
	Node *node = this->headNode;

	for (int i = 0; i < pos; i++) {
		node = node->next;
	}

	return node->data;
}

TList::~TList() {
	printf("Destructor\n");
	this->reset();
}