#include <iostream>
#include <cstdio>
#include "TList.h"

using namespace std;

TList getReverseList(const TList lstSrc);			   // Sin mejora.
void  getReverseList(const TList &lstSrc, TList *rev); // Con mejora.

int main() {
	TList list;
	TList rev;

	list.push("Miguel");
	list.push("Hola");
	list.push("Ey");

	getReverseList(list, &rev);
	//rev = getReverseList(list);

	for (int i = 0; i < (rev).size(); i++) {
		printf("%s", (rev).at(i));
	}

	printf("\n");

	getchar();

	return 0;
}

// CON MEJORA (no se llama al constructor de copia).
void getReverseList(const TList &lstSrc, TList *rev) {
	const char *temp;

	for (int i = lstSrc.size() - 1; i >= 0; i--) {
		temp = lstSrc.at(i);
		rev->push(temp);
	}
}

// SIN MEJORA.
TList getReverseList(const TList lstSrc) {
	TList rev;
	const char *temp;

	for (int i = lstSrc.size() - 1; i >= 0; i--) {
		temp = lstSrc.at(i);
		rev.push(temp);
	}

	return rev;
}