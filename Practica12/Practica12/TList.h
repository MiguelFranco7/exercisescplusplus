struct  Node {
	char *data;
	Node *next;
};

class TList {
public:
	void		Init           ();
				TList          ();
	            TList          (const TList &other);
	int         size           () const;
	int         push           (const char*);
	const char *first          () const;
	const char *next           () const;
	const char *pop            ();
	const char *at             (int pos) const;
	void        reset          ();
	           ~TList          ();

private:
	int   count;
	Node *headNode;
};