#include "TList.h"
#include <cstdio>
#include <stdio.h>

void TList::Init() {
	this->count = 0;
	this->headNode = nullptr;
	this->nextElement = nullptr;
}

int TList::size() {
	return this->count;
}

int TList::push(const char *p) {
	Node *pNew = new Node;
	Node *pPre = nullptr;
	Node *pCur = this->headNode;
	int len = 0;

	while (*(p + len) != NULL) {
		len++;
	}

	pNew->data = new char[len+1];
	for (int i = 0; i < len; i++) {
		pNew->data[i] = *(p + i);
	}

	pNew->data[len] = '\0';

	while (pCur) {
		pPre = pCur;
		pCur = pCur->next;
	}

	if (pPre) {
		pNew->next = pPre->next;
		pPre->next = pNew;
		this->count++;
	} else {
		pNew->next = this->headNode;
		this->headNode = pNew;
		this->nextElement = this->headNode;
		this->count++;
	}

	return this->count;
}

const char * TList::first() {
	if (this->count > 0) {
		return this->headNode->data;
	} else
		return nullptr;
}

const char * TList::next() {
	if (this->nextElement->next != nullptr) {
		this->nextElement = this->nextElement->next;
		return this->nextElement->data;
	} else
		return nullptr;
}

const char * TList::pop() {
	if (this->count > 0) {
		const char *data = this->headNode->data;
		Node *temp = this->headNode;

		if (this->nextElement == this->headNode)
			this->nextElement = this->headNode->next;

		this->headNode = this->headNode->next;
		this->count--;
		delete temp;

		return data;
	} else 
		return nullptr;
}

void TList::reset() {
	char *p;
	Node *n;

	while (this->headNode) {
		p = this->headNode->data;
		n = this->headNode;
		delete p;

		this->headNode = this->headNode->next;
		delete n;
		this->count--;
	}
}

TList::~TList() {
	this->reset();
}