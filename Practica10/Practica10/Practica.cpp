#include <iostream>
#include "TList.h"

using namespace std;

int main(int argc, char* argv[]) {
	TList *lista = new TList();
	lista->Init();

	cout << "Size: " << lista->size() << endl;

	lista->push("Hola Miguel");
	lista->push("Hola Hola");
	lista->push("Hola");

	const char *first = lista->first();
	const char *next = lista->next();
	const char *pop = lista->pop();

	if (first != nullptr) {
		int len = 0;
		cout << "First: " << first << endl;
	}

	if (next != nullptr) {
		int len = 0;
		cout << "Next: " << next << endl;
	}

	if (pop != nullptr) {
		int len = 0;
		cout << "Pop: " << pop << endl;
	}

	lista->push("Miguel");

	cout << "Size: " << lista->size() << endl;

	delete lista;

	getchar();

	return 0;
}