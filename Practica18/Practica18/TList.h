template <class T> struct Node {
	T *data;
	Node<T> *next;
};

template <typename T> class TList {
public:
	void		Init();
	int         size  ();
	int         push  (const T*);
	const T		*first ();
	const T		*next  ();
	const T		*pop   ();
	void        reset ();
	           ~TList ();

private:
	int   count;
	Node<T> *headNode;
	Node<T> *nextElement;
};

template <class T>
inline void TList<T>::Init() {
	this->count = 0;
	this->headNode = nullptr;
	this->nextElement = nullptr;
}

template <class T>
inline int TList<T>::size() {
	return this->count;
}

template <class T>
inline int TList<T>::push(const T *p) {
	Node<T> *pNew = new Node<T>;
	Node<T> *pPre = nullptr;
	Node<T> *pCur = this->headNode;
	int len = 0;

	while (*(p + len) != NULL) {
		len++;
	}

	pNew->data = new T [len + 1];
	for (int i = 0; i < len; i++) {
		pNew->data[i] = *(p + i);
	}

	pNew->data[len] = '\0';

	while (pCur) {
		pPre = pCur;
		pCur = pCur->next;
	}

	if (pPre) {
		pNew->next = pPre->next;
		pPre->next = pNew;
		this->count++;
	}
	else {
		pNew->next = this->headNode;
		this->headNode = pNew;
		this->nextElement = this->headNode;
		this->count++;
	}

	return this->count;
}

template <class T>
inline const T * TList<T>::first() {
	if (this->count > 0) {
		return this->headNode->data;
	}
	else
		return nullptr;
}

template <class T>
inline const T * TList<T>::next() {
	if (this->nextElement->next != nullptr) {
		this->nextElement = this->nextElement->next;
		return this->nextElement->data;
	}
	else
		return nullptr;
}

template <class T>
inline const T * TList<T>::pop() {
	if (this->count > 0) {
		const T *data = this->headNode->data;
		Node<T> *temp = this->headNode;

		if (this->nextElement == this->headNode)
			this->nextElement = this->headNode->next;

		this->headNode = this->headNode->next;
		this->count--;
		delete temp;

		return data;
	}
	else
		return nullptr;
}

template <class T>
inline void TList<T>::reset() {
	T *p;
	Node<T> *n;

	while (this->headNode) {
		p = this->headNode->data;
		n = this->headNode;
		delete p;

		this->headNode = this->headNode->next;
		delete n;
		this->count--;
	}
}

template<class T>
inline TList<T>::~TList() {
	this->reset();
}