// Practica 2.

#include <stdio.h>

int main(int argc, char* argv[])
{
	// PRACTICAS.
	// 1.- Dado un entero obtener por separado cada uno de sus bytes usando operaciones de punteros. 
	// 2.- Dada la tabla de enteros int tabla[] = {1, 3, 2, 5, 3, 0xFFFFFFFF, 2}
	//		 Realizar c�digo que obtenga el entero mayor usando aritm�tica de punteros.
	// 3.- Dada la tabla de enteros char tabla[] = {'a', 'z', 'f', 'g', '2'}
	//		 Realizar c�digo que obtenga el caracter mayor usando aritm�tica de punteros.
	// 4.- Igual que el 2 pero que obtenga la posici�n del byte mayor.
	// 5.- Generar c�digo que dada una cadena le de la vuelta: ejmp "hola" -> "aloh"

	// 1-
	printf(" 1- \n");
	int var1 = 515;
	const char *p = reinterpret_cast<char *>(&var1);

	printf("%d, %d, %d, %d \n", *p, *(p+1), *(p+2), *(p+3));

	// 2-
	printf("\n 2- \n");
	int tabla[] = {1, 3, 2, 5, 3, static_cast<int>(0xFFFFFFFF), 2};
	const int *p2 = tabla;
	int mayor = 0;
    
	for (int i = 1; i < sizeof(tabla)/4; i++) {
		printf("Compara: %d -- %d \n", *(p2+i), *(p2+mayor));
		if (*(p2+i) > *(p2+mayor)) {
			mayor = i;
		}
	}

	printf("Mayor: %d, posicion --> %d \n", *(p2+mayor), mayor);

	// 3-
	printf("\n 3- \n");
	char tabla3[] = {'a', 'z', 'f', 'g', '2'};
	const char *p3 = tabla3;
	int mayor3 = 0;
    
	for (int i = 1; i < sizeof(tabla3); i++) {
		printf("Compara: %d -- %d \n", *(p3+i), *(p3+mayor3));
		if (*(p3+i) > *(p3+mayor3)) {
			mayor3 = i;
		}
	}

	printf("Mayor: %d, posicion --> %d \n", *(p3+mayor3), mayor3);

	// 4- Modelo para modificar los anteriores.
	printf("\n 4- \n");
	int tabla4[] = { 1, 3, 2, 5, 3, static_cast<int>(0xFFFFFFFF), 2 };
	unsigned char *p4 = reinterpret_cast<unsigned char *>(tabla4);
	int mayor4 = 0;

	for (int i = 1; i < sizeof(tabla4); i++) {
		printf("Compara: %d -- %d \n", *(p4 + mayor4), *(p4 + i));
		if (*(p4 + mayor4) < *(p4 + i)) {
			mayor4 = i;
		}
	}

	printf("Mayor: %d, posicion --> %d \n", *(p4 + mayor4), mayor4);

	// 5-
	printf("\n 5- \n");
    char tabla5[] = "Miguel";
    const char *p5 = tabla5;
    
    int i = 0;
    int j = sizeof(tabla5);
    char tablaResult[sizeof(tabla5)];
    
    j--;
    tablaResult[j] = '\0';
    j--;
    
    while (i <= j) {
        tablaResult[i] = *(p5+j);
        tablaResult[j] = *(p5+i);
        i++;
        j--;
    }
    
    for(int z = 0; z < sizeof(tablaResult); z++) {
        printf("%c \n", tablaResult[z]);
    }

	getchar();

	return 0;
}