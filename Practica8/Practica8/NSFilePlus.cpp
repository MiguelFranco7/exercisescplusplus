#include <stdlib.h>
#include <string.h>
#include "NSFile.h"
#include "NSFilePlus.h"

const int MaxBufferSize = 5;

int NSFilePlus::GetNumStringTimes(const char *pszFileName, const char *psz) {
	int uRet = 0;
	if (psz && pszFileName) {
		int iIdFile = NSFile::OpenFile(pszFileName, NSFile::EFileMode_Read);

		if (iIdFile != NSFile::ERROR) {
			char Buffer[MaxBufferSize + 1];
			unsigned int iLen = strlen(psz);
			if (iLen <= MaxBufferSize) {
				unsigned int uBytesRead = 0;
				
				uBytesRead = NSFile::ReadFile(iIdFile, reinterpret_cast<unsigned char *>(Buffer), MaxBufferSize);

				if (uBytesRead)
					Buffer[uBytesRead] = '\0';

				unsigned int iDesp = iLen - 1;
				while (uBytesRead > 0) {
					const char *pFound = strstr(Buffer, psz);

					while (pFound) {
						uRet++;
						pFound++;
						pFound = strstr(pFound, psz);
					}

					for (unsigned int i = 0; i < iDesp; i++)
						Buffer[i] = Buffer[MaxBufferSize - iLen + 1 + i];

					uBytesRead = NSFile::ReadFile(iIdFile, reinterpret_cast<unsigned char *>(Buffer + iDesp), MaxBufferSize - iDesp);

					Buffer[iDesp + uBytesRead] = '\0';
				}
			}
			NSFile::CloseFile(iIdFile);
		}
	}

	return uRet;
}

int	NSFilePlus::SumFileNumbers(const char *pszFileName) {
	int uRet = 0;
	if (pszFileName) {
		int iIdFile = NSFile::OpenFile(pszFileName, NSFile::EFileMode_Read);

		if (iIdFile != NSFile::ERROR) {
			char Buffer[MaxBufferSize + 1];
			unsigned int uBytesRead = 0;

			uBytesRead = NSFile::ReadFile(iIdFile, reinterpret_cast<unsigned char *>(Buffer), MaxBufferSize);

			if (uBytesRead)
				Buffer[uBytesRead] = '\0';

			while (uBytesRead > 0) {
				const char *pIni = Buffer;
				char *pFound = strstr(Buffer, ",");

				while (pFound) {
					*pFound = '\0';
					uRet += atoi(pIni);
					pFound++;
					pIni = pFound;
					pFound = strstr(pFound, ",");
				}

				int iCount = (Buffer + MaxBufferSize) - pIni;
				for (int i = 0; i < iCount; i++)
					Buffer[i] = pIni[i];

				uBytesRead = NSFile::ReadFile(iIdFile, reinterpret_cast<unsigned char *>(Buffer + iCount), MaxBufferSize - iCount);

				Buffer[iCount + uBytesRead] = '\0';
			}

			uRet += atoi(Buffer);

			NSFile::CloseFile(iIdFile);
		}
	}

	return uRet;
}