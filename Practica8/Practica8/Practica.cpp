#include <iostream>
#include <cstdio>
#include "NSFile.h"
#include "NSFilePlus.h"

using namespace std;

int main(int argc, char* argv[]) {

	int id = NSFile::OpenFile("Prueba.txt", NSFile::EFileMode_Read);

	unsigned int lSize = NSFile::FileSize(id);
	unsigned char *buffer = new unsigned char[lSize];
	buffer[lSize] = '\0';

	unsigned int numCar = NSFile::ReadFile(id, buffer, lSize);
	NSFile::CloseFile(id);

	cout << "NumCar: " << numCar << endl << buffer << endl;

	int id1 = NSFile::OpenFile("Prueba.txt", NSFile::EFileMode_Write);
	const char *buffer1 = "Holaa Miguel";
	numCar = NSFile::WriteFile(id, buffer1, strlen(buffer1));
	NSFile::CloseFile(id1);

	cout << "NumCar: " << numCar << endl << buffer1 << endl;

	int ap = NSFilePlus::GetNumStringTimes("Prueba1.txt", "Hola");
	int sum = NSFilePlus::SumFileNumbers("Prueba2.txt");

	printf("%d, %d\n", ap, sum);

	getchar();

	return 0;
}